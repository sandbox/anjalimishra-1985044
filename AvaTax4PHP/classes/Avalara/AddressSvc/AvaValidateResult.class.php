<?php
/**
 * AvaValidateResult.class.php
 */

/**
 * Contains an array of {@link AvaValidAddress} objects returned by {@link AvaAddressServiceSoap#validate}
 *
 * <pre>
 *  $port = new AvaAddressServiceSoap();
 *  use Avalara\AddressSvc\AvaAddress as AvaAddress;
 *
 *  $address = new AvaAddress();
 *  $address->setLine1("900 Winslow Way");
 *  $address->setLine2("Suite 130");
 *  $address->setCity("Bainbridge Is");
 *  $address->setRegion("WA");
 *  $address->setPostalCode("98110-2450");
 *
 *  $result = $port->validate($address,AvaTextCase::$Upper);
 *  $addresses = $result->ValidAddresses;
 *  print("Number of addresses returned is ". sizeoof($addresses));
 *
 * </pre>
 *
 * @see ValidAddress
 *
 * @author    Avalara
 * @copyright 2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AddressSvc
 */

namespace Avalara\AddressSvc {
    class AvaValidateResult// extends AvaBaseResult
    {
        /**
         * Array of matching {@link AvaValidAddress}'s.
         * @var array
         */
        private $ValidAddresses;

        /**
         * Method returning array of matching {@link AvaValidAddress}'s.
         * @return array
         */
        public function getValidAddresses() { return EnsureIsArray($this->ValidAddresses->ValidAddress); }





        /**
         * @var string
         */
        private $TransactionId;
        /**
         * @var string must be one of the values defined in {@link AvaSeverityLevel}.
         */
        private $ResultCode = 'Success';
        /**
         * @var array of Message.
         */
        private $Messages = array();

        /**
         * Accessor
         * @return string
         */
        public function getTransactionId() { return $this->TransactionId; }
        /**
         * Accessor
         * @return string
         */
        public function getResultCode() { return $this->ResultCode; }
        /**
         * Accessor
         * @return array
         */
        public function getMessages() { return EnsureIsArray($this->Messages->Message); }

        //@author:swetal

        public function isTaxable()
        {
            return $this->Taxable;
        }

    }

}