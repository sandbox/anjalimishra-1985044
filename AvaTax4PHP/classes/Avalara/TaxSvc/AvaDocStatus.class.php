<?php
/**
 * AvaDocStatus.class.phpphp
 */

/**
 * The document's status is returned in the AvaGetTaxResult (except for <b>AvaDocStatus::$Any</b>)
 * and indicates the state of the document in tax history.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */

namespace Avalara\TaxSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaDocStatus extends AvaEnum
    {

        /**
         * A temporary document not saved (AvaDocumentType was SalesOrder, PurchaseOrder, ReturnOrder)
         *
         * @var unknown_type
         */
        public static $Temporary= 'Temporary';

        /**
         *  A saved document (AvaDocumentType was SalesInvoice, PurchaseInvoice, ReturnInvoice) ready to be posted.
         *
         * @var AvaDocStatus
         */
        public static $Saved	= 'Saved';

        /**
         *  A posted document (not committed).
         *
         * @var AvaDocStatus
         */
        public static $Posted	= 'Posted';

        /**
         *  A posted document that has been committed.
         *
         * @var AvaDocStatus
         */
        public static $Committed	= 'Committed';

        /**
         *  A committed document that has been cancelled.
         *
         * @var AvaDocStatus
         */
        public static $Cancelled	= 'Cancelled';

        /**
         * Enter description here...
         *
         * @var AvaDocStatus
         */
        public static $Adjusted	= 'Adjusted';

        /**
         * Any status (used for searching)
         *
         * @var AvaDocStatus
         */
        public static $Any		= 'Any';


        public static function Values()
        {
            return array(
                AvaDocStatus::$Temporary,
                AvaDocStatus::$Saved,
                AvaDocStatus::$Posted,
                AvaDocStatus::$Committed,
                AvaDocStatus::$Cancelled,
                AvaDocStatus::$Adjusted,
                AvaDocStatus::$Any
            );
        }
        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}