<?php
/**
 * AvaDetailLevel.class.phpphp
 */

/**
 * Specifies the level of tax detail to return to the client application following a tax calculation.
 * @see GetTaxRequest, GetTaxHistoryRequest
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */

namespace Avalara\TaxSvc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaDetailLevel extends AvaEnum
    {
        /**
         * Reserved for future use.
         */
        public static $Summary		= 'Summary';

        /**
         *  Document ({@link AvaGetTaxResult}) level details; {@link ArrayOfTaxLine} will not be returned.
         */
        public static $Document		= 'Document';

        /**
         *  Line level details (includes Document details). {@link ArrayOfTaxLine} will
         * be returned but {@link ArrayOfTaxDetail} will not be returned.
         */
        public static $Line			= 'Line';

        /**
         *  Tax jurisdiction level details (includes Document, {@link ArrayOfTaxLine},
         * and {@link ArrayOfTaxDetail})
         */
        public static $Tax			= 'Tax';
        public static $Diagnostic	= 'Diagnostic';


        public static function Values()
        {
            return array(
                AvaDetailLevel::$Summary,
                AvaDetailLevel::$Document,
                AvaDetailLevel::$Line,
                AvaDetailLevel::$Tax,
                AvaDetailLevel::$Diagnostic
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }

}