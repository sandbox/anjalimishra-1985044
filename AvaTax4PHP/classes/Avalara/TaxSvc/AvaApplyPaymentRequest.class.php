<?php
/**
 * AvaApplyPaymentRequestest.class.php
 */

/**
 * AvaApplyPaymentRequest.class.phpphp
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaApplyPaymentRequest
    {
        private $CompanyCode;   //string
        private $DocType;       //AvaDocumentType
        private $DocCode;       //string
        private $PaymentDate;   //date


        public function __construct()
        {
            $this->DocType=AvaDocumentType::$SalesOrder;
        }

        /**
         * Sets the companyCode value for this AvaApplyPaymentRequest.
         *
         * @param string $value
         */
        public function setCompanyCode($value){ $this->CompanyCode=$value;}   //string

        /**
         * Sets the docCode value for this AvaApplyPaymentRequest.
         *
         * @param AvaDocumentType $value
         */
        public function setDocType($value){ $this->DocType=$value;}       //AvaDocumentType

        /**
         * Sets the docType value for this AvaApplyPaymentRequest.
         *
         * @param string $value
         */
        public function setDocCode($value){ $this->DocCode=$value;}       //string

        /**
         * PaymentDate should be in the format yyyy-mm-dd
         *
         * @param date $value
         */
        public function setPaymentDate($value){ $this->PaymentDate=$value;}   //date


        /**
         * Gets the companyCode value for this AvaApplyPaymentRequest.
         *
         * @return string
         */
        public function getCompanyCode(){ return $this->CompanyCode;}   //string

        /**
         * Gets the docType value for this AvaApplyPaymentRequest.
         *
         * @return AvaDocumentType
         */
        public function getDocType(){ return $this->DocType;}       //AvaDocumentType

        /**
         *  Gets the docCode value for this AvaApplyPaymentRequest.
         *
         * @return unknown
         */
        public function getDocCode(){ return $this->DocCode;}       //string
        /**
         * PaymentDate should be in the format yyyy-mm-dd
         *
         * @param date $value
         */
        public function getPaymentDate(){ return $this->PaymentDate;}   //date

    }
}