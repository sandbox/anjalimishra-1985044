<?php
/**
 * AvaTaxTypeype.class.php
 */

/**
 * The Type of the tax.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaTaxType// extends AvaEnum
    {
        public static $Sales	= 'Sales';
        public static $Use		= 'Use';
        public static $ConsumerUse	= 'ConsumerUse';
        public static $Excise ='Excise';
        /*
        public static function Values()
        {
            return array(
                $AvaTaxType::$Sales,
                $AvaTaxType::$Use,
                $AvaTaxType::$ConsumerUse
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

        */

    }


}