<?php
/**
 * AvaTaxOverrideType.class.phpphp
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   TaxSvc
 */
namespace Avalara\TaxSvc {
    class AvaTaxOverrideType
    {
        public static $None = "None";
        public static $TaxAmount = "TaxAmount";
        public static $Exemption = "Exemption";
        public static $TaxDate = "TaxDate";
        public static $AccruedTaxAmount = "AccruedTaxAmount";
    }

}