<?php
/**
 * AvaBatchFileSaveResponse.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFileSaveResponse {
        private $BatchFileSaveResult; // BatchFileSaveResult

        public function setBatchFileSaveResult($value){$this->BatchFileSaveResult=$value;} // BatchFileSaveResult
        public function getBatchFileSaveResult(){return $this->BatchFileSaveResult;} // BatchFileSaveResult

    }

}