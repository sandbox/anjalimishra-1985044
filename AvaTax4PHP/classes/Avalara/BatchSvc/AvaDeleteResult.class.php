<?php
/**
 * AvaDeleteResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaDeleteResult extends AvaBaseResult {

    }

}