<?php
/**
 * AvaBatchSaveResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaBatchSaveResult extends AvaBaseResult {
        private $BatchId; // int
        private $EstimatedCompletion; // dateTime

        public function setBatchId($value){$this->BatchId=$value;} // int
        public function getBatchId(){return $this->BatchId;} // int

        public function setEstimatedCompletion($value){$this->EstimatedCompletion=$value;} // dateTime
        public function getEstimatedCompletion(){return $this->EstimatedCompletion;} // dateTime

    }

}