<?php
/**
 * AvaBatchFileFetchResult.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaBatchFileFetchResult extends AvaBaseResult {
        private $BatchFiles; // ArrayOfBatchFile
        private $RecordCount; // int

        public function setBatchFiles($value){$this->BatchFiles=$value;} // ArrayOfBatchFile
        public function getBatchFiles(){return $this->BatchFiles;} // ArrayOfBatchFile

        public function setRecordCount($value){$this->RecordCount=$value;} // int
        public function getRecordCount(){return $this->RecordCount;} // int

    }

}