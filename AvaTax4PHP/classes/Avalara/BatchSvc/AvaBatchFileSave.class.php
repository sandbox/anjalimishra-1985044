<?php
/**
 * AvaBatchFileSave.class.php
 */

/**
 *
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BatchSvc
 */
namespace Avalara\BatchSvc {
    class AvaBatchFileSave {
        private $BatchFile; // BatchFile

        public function setBatchFile($value){$this->BatchFile=$value;} // BatchFile
        public function getBatchFile(){return $this->BatchFile;} // BatchFile

    }

}