<?php
/**
 * AvaCertificateRequest.class.php
 */

/**
 * Contains certificate request data.  Is part of the {@link AvaCertificateRequestGetResult} result came from the {@link CertificateRequestGet}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    class AvaCertificateRequest {
        private $RequestId; // string
        private $TrackingCode; // string
        private $SourceLocationCode; // string
        private $RequestDate; // dateTime
        private $CustomerCode; // string
        private $CreatorName; // string
        private $LastModifyDate; // dateTime
        private $RequestStatus; // AvaCertificateRequestStatus
        private $RequestStage; // AvaCertificateRequestStage
        private $CommunicationMode; // AvaCommunicationMode

        /**
         * Unique identifier for the certificate request record.
         */
        public function getRequestId(){return $this->RequestId;} // string

        /**
         * Unique Tracking Code for the certificate request record.
         */
        public function getTrackingCode(){return $this->TrackingCode;} // string

        /**
         * Source location code for the certificate record (the client location responsible for tracking the certificate).
         */
        public function getSourceLocationCode(){return $this->SourceLocationCode;} // string

        /**
         * Request date of the certificate request record.
         */
        public function getRequestDate(){return $this->RequestDate;} // dateTime

        /**
         * Customer identification code for the customer associated with the certificate request record.
         */
        public function getCustomerCode(){return $this->CustomerCode;} // string

        /**
         * CreatorName the certificate request record.
         */
        public function getCreatorName(){return $this->CreatorName;} // string

        /**
         * Last modification date of the certificate request record.
         */
        public function getLastModifyDate(){return $this->LastModifyDate;} // dateTime

        /**
         * Request status for the certificate request record.
         */
        public function getRequestStatus(){return $this->RequestStatus;} // AvaCertificateRequestStatus

        /**
         * Request stage for the certificate request record.
         */
        public function getRequestStage(){return $this->RequestStage;} // AvaCertificateRequestStage

        /**
         * CommunicationMode for the certificate request record.
         */
        public function getCommunicationMode(){return $this->CommunicationMode;} // CommunicationMode

    }

 }