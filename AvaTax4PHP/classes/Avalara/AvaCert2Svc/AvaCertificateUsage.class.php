<?php
/**
 * AvaCertificateUsage.class.php
 */

/**
 * AvaCertificateUsage indicates the usage type for the Certificate record.
 * @see ExemptionCertificate
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaCertificateUsage extends AvaEnum
    {
        /**
         *  The certificate may be used multiple times.
         *
         * @var AvaCertificateUsage
         */
        public static $BLANKET	= 'BLANKET';

        /**
         *  The certificate may only be used for a single transaction.
         *
         * @var AvaCertificateUsage
         */
        public static $SINGLE	= 'SINGLE';

        /**
         *  The value has not been set.
         *
         * @var AvaCertificateUsage
         */
        public static $NULL	= 'NULL';

        public static function Values()
        {
            return array(
                AvaCertificateUsage::$BLANKET,
                AvaCertificateUsage::$SINGLE,
                AvaCertificateUsage::$NULL
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }

    }

}