<?php
/**
 * AvaCertificateGetResult.class.php
 */

/**
 * Contains the get exemption certificates operation result returned by {@link CertificateGet}.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaBaseResult as AvaBaseResult;
    class AvaCertificateGetResult extends AvaBaseResult {
        private $Certificates; // ArrayOfCertificate

        /**
         * Certificates contains collection of exemption certificate records.
         */
        public function getCertificates()
        {
            if(isset($this->Certificates->Certificate))
            {
                return EnsureIsArray($this->Certificates->Certificate);
            }
            else
            {
                return null;
            }
        } // ArrayOfCertificate

    }

}