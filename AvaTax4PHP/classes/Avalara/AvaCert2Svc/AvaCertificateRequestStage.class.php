<?php
/**
 * AvaCertificateRequestStage.class.php
 */

/**
 * AvaCertificateStatus indicates the current stage of the Request.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   AvaCert2Svc
 */
namespace Avalara\AvaCert2Svc {
    use Avalara\BaseSvc\AvaEnum as AvaEnum;
    class AvaCertificateRequestStage extends AvaEnum {

        /**
         * Request has been initiated; correspondence has been sent to the associated Customer.
         */
        public static $REQUESTINITIATED = 'REQUESTINITIATED';

        /**
         * Customer has responded to the correspondence.
         */
        public static $CUSTOMERRESPONDED = 'CUSTOMERRESPONDED';

        /**
         * Customer has provided a Certificate.
         */
        public static $CERTIFICATERECEIVED = 'CERTIFICATERECEIVED';

        public static function Values()
        {
            return array(
                AvaCertificateRequestStage::$REQUESTINITIATED,
                AvaCertificateRequestStage::$CUSTOMERRESPONDED,
                AvaCertificateRequestStage::$CERTIFICATERECEIVED
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }

}