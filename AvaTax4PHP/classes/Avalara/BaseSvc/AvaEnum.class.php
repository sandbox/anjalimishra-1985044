<?php
/**
 * AvaEnum.class.php
 */

/**
 * Abstract class for enumerated types - provides validation.
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */
namespace Avalara\BaseSvc {
    class AvaEnum
    {
        // Basic implementation - check and throw
        protected static function __Validate($value,$values,$class=__CLASS__)
        {
            foreach($values as $valid)
            {
                if($value == $valid)
                {
                    return true;
                }
            }

            throw new Exception('Invalid '.$class.' "'.$value.'" - must be one of "'.implode('"|"',$values).'"');
        }
    }

}