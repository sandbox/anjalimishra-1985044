<?php
/**
 * AvaSeverityLevel.class.php
 */

/**
 * Severity of the result {@link AvaMessage}.
 *
 * Defines the constants used to specify AvaSeverityLevel in {@link AvaMessage}
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */

namespace Avalara\BaseSvc {

    class AvaSeverityLevel extends AvaEnum
    {
        public static $Success = 'Success';
        public static $Warning = 'Warning';
        public static $Error = 'Error';
        public static $Exception = 'Exception';


        public static function Values()
        {
            return array(
                AvaSeverityLevel::$Success,
                AvaSeverityLevel::$Warning,
                AvaSeverityLevel::$Error,
                AvaSeverityLevel::$Exception
            );
        }

        // Unfortunate boiler plate due to polymorphism issues on static functions
        public static function Validate($value) { self::__Validate($value,self::Values(),__CLASS__); }
    }

}