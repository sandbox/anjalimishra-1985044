<?php
/**
 * AvaPingResult.class.phpphp
 */

/**
 * Result information returned from the {@link AvaAddressServiceSoap}'s
 * {@link AvaAddressServiceSoap#ping} method and the {@link AvaTaxServiceSoap}'s
 * {@link AvaTaxServiceSoap#ping} method.
 * <b>Example:</b><br>
 * <pre>
 *  $svc = new AvaAddressServiceSoap();
 *
 *  $result = svc->ping();
 *  $numMessages = sizeof($result->getMessages());
 *
 * </pre>
 *
 * @author    Avalara
 * @copyright   2004 - 2013 Avalara, Inc.  All rights reserved.
 * @package   BaseSvc
 */

namespace Avalara\BaseSvc {
    class AvaPingResult //extends AvaBaseResult
    {
        /**
         * Version string of the pinged service.
         * @var string
         */
        private $Version;

        /**
         * Method returning version string of the pinged service.
         * @return string
         */
        public function getVersion() { return $this->Version; }

// AvaBaseResult innards - workaround a bug in SoapClient

        /**
         * @var string
         */
        private $TransactionId;
        /**
         * @var string must be one of the values defined in {@link AvaSeverityLevel}.
         */
        private $ResultCode = 'Success';
        /**
         * @var array of Message.
         */
        private $Messages = array();

        /**
         * Accessor
         * @return string
         */
        public function getTransactionId() { return $this->TransactionId; }
        /**
         * Accessor
         * @return string
         */
        public function getResultCode() { return $this->ResultCode; }
        /**
         * Accessor
         * @return array
         */
        public function getMessages() { return EnsureIsArray($this->Messages->Message); }



    }

}